from flask import Flask
from flask_pymongo import PyMongo
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_cors import CORS
import datetime


app = Flask(__name__)

app.config[
    "MONGO_URI"
] = "mongodb+srv://admin:admin@cluster0.0z42a.gcp.mongodb.net/test?retryWrites=true&w=majority"
app.config["JWT_SECRET_KEY"] = "secret"

# Configure application to store JWTs in cookies. Whenever you make
# a request to a protected endpoint, you will need to send in the
# access or refresh JWT via a cookie.
app.config['JWT_TOKEN_LOCATION'] = ['cookies']

app.config['JWT_ACCESS_TOKEN_EXPIRES'] = False #secs
# app.config['JWT_REFRESH_TOKEN_EXPIRES'] = 60 #secs

# Disable CSRF protection for this example. In almost every case,
# this is a bad idea. See examples/csrf_protection_with_cookies.py
# for how safely store JWTs in cookies
app.config['JWT_COOKIE_CSRF_PROTECT'] = False

mongo = PyMongo(app)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)
CORS(app)