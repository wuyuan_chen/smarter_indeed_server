from flask import Flask, jsonify, request, json, Blueprint
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
from datetime import datetime, timedelta, timezone
from flask_bcrypt import Bcrypt
from flask_jwt_extended import (JWTManager, set_access_cookies, set_refresh_cookies, get_jwt_identity, jwt_refresh_token_required, \
                create_access_token, create_refresh_token, unset_jwt_cookies)
from resources import mongo, bcrypt, jwt

login_sys = Blueprint('login_sys', __name__)

@login_sys.route("/users/register", methods=["POST"])
def register():
    users = mongo.db.users
    email = request.get_json()["email"]
    response = users.find_one({"email": email})
    if response:
        result = jsonify({"result": "email already registered"})
        return result

    first_name = request.get_json()["first_name"]
    last_name = request.get_json()["last_name"]

    password = bcrypt.generate_password_hash(request.get_json()["password"]).decode(
        "utf-8"
    )
    created = datetime.utcnow()

    user_id = users.insert(
        {
            "first_name": first_name,
            "last_name": last_name,
            "email": email,
            "password": password,
            "created": created,
        }
    )

    new_user = users.find_one({"_id": user_id})

    result = {"email": new_user["email"] + " registered"}

    return jsonify({"result": result})


@login_sys.route("/users/login", methods=["POST"])
def login():
    users = mongo.db.users
    email = request.get_json()["email"]
    password = request.get_json()["password"]
    result = ""

    db_user_info = users.find_one({"email": email})

    if db_user_info:
        if bcrypt.check_password_hash(db_user_info["password"], password):
            print(get_jwt_identity())
            current_user = {
                "first_name":db_user_info["first_name"],
                "last_name": db_user_info["last_name"],
                "email": email,
            }
            access_token = create_access_token(
                identity = current_user,
            )
            refresh_token = create_refresh_token(
                identity = current_user
            )
            data = {
                "accessToken": access_token,
            }
            resp = jsonify(data);
            print(access_token)
            print(current_user)
            set_access_cookies(resp, access_token)
            set_refresh_cookies(resp, refresh_token)
        else:
            resp = jsonify({"error": "Invalid username and password"})
    else:
        resp = jsonify({"result": "No results found"})
    return resp

# Same thing as login here, except we are only setting a new cookie
# for the access token.
@login_sys.route('/token/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    # Create the new access token
    current_user = get_jwt_identity()
    print("current_user->{}".format(current_user))
    access_token = create_access_token(identity=current_user)
    data = {
            "accessToken": access_token,
            }
    # Set the JWT access cookie in the response
    resp = jsonify({'refresh': True})
    set_access_cookies(resp, access_token)
    return resp, 200


# Because the JWTs are stored in an httponly cookie now, we cannot
# log the user out by simply deleting the cookie in the frontend.
# We need the backend to send us a response to delete the cookies
# in order to logout. unset_jwt_cookies is a helper function to
# do just that.
@login_sys.route('/token/remove', methods=['POST'])
def logout():
    resp = jsonify({'logout': True})
    unset_jwt_cookies(resp)
    return resp, 200

