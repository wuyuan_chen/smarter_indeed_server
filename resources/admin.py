from flask import Blueprint, jsonify, request
from flask_jwt_extended import jwt_required
from resources import mongo

admin = Blueprint('admin', '__main__')


def __get_user_info():
    users = mongo.db.users
    # print(list(request.get_json()))
    email = request.get_json()["email"]
    email_info = {"email": email}
    db_user_info = users.find_one(email_info)
    return db_user_info

@admin.route("/admin/getbookmarks", methods=["POST"])
@jwt_required
def get_bookmarks():
    response = {}
    db_user_info = __get_user_info()
    if db_user_info:
        email = db_user_info["email"]
        bookmarks = mongo.db.bookmarks

        bookmark_info = bookmarks.find_one({"email": email})
        if("bookmarks" in bookmark_info):
            response["bookmarks"] = bookmark_info["bookmarks"]
            return response
    response["bookmarks"] = []
    return response
    
@admin.route("/admin/removepost", methods=["POST"])
@jwt_required
def remove_post():
    response = {}
    db_user_info = __get_user_info()
    if db_user_info:
        print("---> {}".format(request.get_json()))
        bookmarks = mongo.db.bookmarks
        email = db_user_info["email"]
        postId = request.get_json()["id"]
        company = request.get_json()["company"] 
        title = request.get_json()["title"]
        date = request.get_json()["date"]
        location = request.get_json()["location"]
        summary = request.get_json()["summary"]

        job_info = {
            "id": postId,
            "company": company,
            "title": title,
            "date": date,
            "location": location,
            "summary": summary,
        }

        bookmark_info = bookmarks.find_one({"email": email})
        if bookmark_info:
            if("bookmarks" not in bookmark_info.keys()):
                bookmark_info["bookmarks"] = []
            
            bm_list = bookmark_info["bookmarks"]
            for i in range(len(bm_list)):
                bm = bm_list[i]
                if(bm["title"] == job_info["title"] and bm["summary"] == job_info["summary"] \
                    and bm["date"] == job_info["date"] and bm["location"] == job_info["location"]):
                    del bm_list[i]
                    bookmarks.update(
                        {"email": email,},
                        {
                            "$set":{"bookmarks": bm_list}
                        })
                    response["result"] = "removed job from bookmark"
                    break
    else:
        response["error"] = "stop hacking to people's account"
    # bookmark_info = bookmarks.find_one({"email": email})
    # response["existBookmark"] = bookmark_info["bookmarks"]
    return response
    

@admin.route("/admin/savepost", methods=["POST"])
@jwt_required
def save_post():
    response = {}
    db_user_info = __get_user_info()
    if db_user_info:
        bookmarks = mongo.db.bookmarks
        email = db_user_info["email"]
        postId = request.get_json()["id"]
        company = request.get_json()["company"]
        title = request.get_json()["title"]
        date = request.get_json()["date"]
        location = request.get_json()["location"]
        summary = request.get_json()["summary"]

        job_info = {
            "id": postId,
            "company": company,
            "title": title,
            "date": date,
            "location": location,
            "summary": summary,
        }

        bookmark_info = bookmarks.find_one({"email": email})
        if bookmark_info:
            bm_exist = False
            if("bookmarks" not in bookmark_info.keys()):
                bookmark_info["bookmarks"] = []
            
            bm_list = bookmark_info["bookmarks"]
            bookmarks.update(
                {"email": email,},
                {
                    "$push":{"bookmarks": job_info}
                })
            response["result"] = "job bookmarked"
        else:
            data = {"email": email,
                    "bookmarks": [job_info]
            }
            response["result"] = "user does exist"
            user_bookmarks = bookmarks.insert(data)
            print("saved post on server {}".format(user_bookmarks))
            
        bookmark_info = bookmarks.find_one({"email": email})
        response["existBookmark"] = bookmark_info["bookmarks"]
    else:
        response["error"] = "stop hacking to people's account"
    return jsonify(response)
