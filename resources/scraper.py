from flask import Flask, Response
from flask import request, Blueprint
from bs4 import BeautifulSoup as bs
import multiprocessing
from multiprocessing import Pool, Manager, Process
import requests
import argparse
import sys
import json
import pdb
import re
# fake_data = "{'jobs': 'writer'}"
fake_data1 = {
    "jobs": [
        {
            "id": "HealthResearchInc.SoftwareEngineerDevelopernewAlbanyNY4daysago",
            "location": "Albany, NY",
            "title": "Software Engineer/Developer\nnew",
            "company": "Health Research, Inc.",
            "date": "4 days ago",
            "summary": "Bachelor\u2019s degree in Computer Science or Information Technology; OR an Associate\u2019s degree in Computer Science, Information Technology or a related field and two\u2026",
            "link": "https://www.indeed.com/rc/clk?jk=088ceb6ec2d53002&fccid=894b8e41dacb3691&vjs=3"
        },
        {
            "id": "IntegriDATACSoftwareDeveloperFinTechSoftwarenewNewYorkNY10168MurrayHillarea4daysago",
            "location": "New York, NY 10168 (Murray Hill area)",
            "title": "C# Software Developer (FinTech Software)\nnew",
            "company": "IntegriDATA",
            "date": "4 days ago",
            "summary": "You will be exposed to proprietary algorithms and methodologies to solve a wide range of financial use cases (expense allocations, collateral management, etc.).",
            "link": "https://www.indeed.com/rc/clk?jk=bd476e3c580ecbad&fccid=02e98d61f725a72b&vjs=3"
        },
        {
            "id": "TXRXSystemsIncIntern-SoftwareEngineeringnewAngolaNY140061dayago",
            "location": "Angola, NY 14006",
            "title": "Intern - Software Engineering\nnew",
            "company": "TX RX Systems Inc",
            "date": "1 day ago",
            "summary": "Validation testing of software-based products.",
            "link": "https://www.indeed.com/rc/clk?jk=c85a84bbeb61b3ba&fccid=7bca63b1586338d7&vjs=3"
        },
        {
            "id": "rootliquidsEntryLevelJavaDevelopernewNewYorkNY10007FinancialDistrictarea2daysago",
            "location": "New York, NY 10007 (Financial District area)",
            "title": "Entry Level Java Developer\nnew",
            "company": "root liquids",
            "date": "2 days ago",
            "summary": "Bachelors or Maters in Computer Science or related area, or relevant work experience.",
            "link": "https://www.indeed.com/rc/clk?jk=84fb505194310737&fccid=8b4eef541e37f57f&vjs=3"
        }
    ]
}

fake_data = json.dumps(fake_data1)

scraper = Blueprint('scraper', __name__)

'''
test on postman, sent a get request
{
    "jobTitle": "software developer",
    "location": "San jose"
}
'''
@scraper.route('/')
def index():
    job_title = request.args.get('jobTitle')
    job_location = request.args.get('location')
    if(job_location is None):
        job_location = "san+jose"
    data = main(job_title, job_location)
    #data = load_json()
    resp = Response(data)
    resp.headers['Access-Control-Allow-Origin'] = "*"
    resp.headers['content_type'] = "json"
    print(resp)
    return resp

def add(x, y=7):
    return x+y


def load_json():
    f = open("job_posts.json")
    data = json.load(f)
    return json.dumps(data)

def find_digits(val):
    return re.findall(r"(\d+)", val)


def clear_file(file_name):
    with open(file_name, 'w'):
        pass


def append_to_file(file_name, data):
    with open(file_name, 'a') as f:
        f.write(data)
        f.close()


def write_to_file(file_name, data):
    with open(file_name, 'w') as f:
        f.write(data)
        f.close()


def get_link(job_post):
    href_str = job_post.find('a', class_="jobtitle turnstileLink")['href']
    return "{}{}".format("https://www.indeed.com", href_str)


def generate_urls(search_title, search_location):
    base_url = 'https://www.indeed.com/jobs?q={}&l={}&start='.format(
        search_title, search_location)
    all_urls = list()
    for i in range(1, 30, 9):
        all_urls.append(base_url + str(i))

    return all_urls


def scrape_indeed(d, url):
    src = requests.get(url).text
    # src = requests.get('https://www.indeed.com/jobs?q=editor&l=San+jose').text
    soup = bs(src, 'lxml')
    job_posts = soup.find_all(class_='result')
    # this is a object that is used to get reassigned to our shared dictionary
    # so that the dictonary will propagate change
    tmp_list = d['jobs']
    for job_post in job_posts:
        # print(job_post, flush=True)
        info = {}

        # write_to_file("job_post.html", job_post.prettify())
        try:
            location = job_post.find('span', class_='location').text
            date_info = job_post.find('span', class_='date').text
            company = job_post.find('span', class_='company').text.strip()

            date_info_list = date_info.split(' ')
            date_list = find_digits(date_info_list[0])
            try:
                date = int(date_list[0])
                # filter out jobs posted from 20+ days ago
                if(date > 20):
                    continue
            except IndexError:
                date = 0
                pass
                # the job is posted today or just now does not have a date

                # write_to_file('job_post.html', job_post.prettify())
            try:
                title = job_post.h2.text.strip()
                summary = job_post.find('div', class_='summary').li.text
                link = get_link(job_post)
                print("title: {}\nlocation: {}\ncompany:{}\ndate: {}\nsummary: {}\nlink: {}\n\n".format(
                    title, location, company, date, summary, link))
                tmp_company = re.sub('[^a-zA-Z0-9-_*.]', '', company)
                tmp_title = re.sub('[^a-zA-Z0-9-_*.]', '', title)
                tmp_location = re.sub('[^a-zA-Z0-9-_*.]', '', location)
                tmp_date_info = re.sub('[^a-zA-Z0-9-_*.]', '', date_info)
                postId = "{}{}{}{}".format(tmp_company, tmp_title, tmp_location, tmp_date_info)
                info["id"] = postId
                info['location'] = location
                info['title'] = title
                info['company'] = company
                info['date'] = date_info
                info['summary'] = summary
                info['link'] = link

                # append all the infomation to the temp list first
                tmp_list.append(info)
            except AttributeError:
                pass
        except AttributeError:
            pass
    # set the list to the shared dictionary to propagate change
    d['jobs'] = tmp_list
    # print(d)

    # json_object = json.dumps(job_dict, indent=4)
    # write_to_file('job_posts.json', json_object)


def main(title, location):
    print("start scraping Indeed...")
    
    all_urls = generate_urls(title, location)
    # print(all_urls)
    # sd = {}
    # sd["jobs"] = []
    # print(all_urls[0])
    # scrape_indeed(sd, all_urls[0])
    # pdb.set_trace()

    manager = Manager()
    shared_dict = manager.dict()
    # d = {'jobs': []} # shared dict will not work with this declaration
    shared_dict['jobs'] = []
    p_job = [Process(target=scrape_indeed, args=(shared_dict, url))
            for url in all_urls]
    [p.start() for p in p_job]
    [p.join() for p in p_job]

    # print(shared_dict)
    posts_dict = shared_dict.copy() 
    postId_list = []
    job_posts = posts_dict["jobs"]
    #remove duplicate dictionary in the list
    posts_dict["jobs"] = [dict(t) for t in {tuple(d.items()) for d in posts_dict["jobs"]}]
    # need to make a copy of the dict because the shared_dict
    # is a proxyDcit object
    json_object = json.dumps(posts_dict, indent=4)
    # write_to_file('job_posts.json', json_object)
    # print(json_object)
    return json_object