from resources import app
from resources.scraper import scraper
from resources.auth import login_sys
from resources.admin import admin

if __name__ == '__main__':
    app.register_blueprint(scraper)
    app.register_blueprint(login_sys)
    app.register_blueprint(admin)
    app.run(debug=True)
