from flask import Flask, request, json
import pytest
from resources import app
from resources import scraper as pyscraper
from resources.scraper import scraper

@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.register_blueprint(scraper)
    with app.test_client() as client:
        yield client

def test_add():
    assert pyscraper.add(7, 3) == 10

@pytest.mark.parametrize("title, location, result", 
                        [
                            ("software+engineer", "san+jose", "software engineer"),
                            ("writer", "new+york", "writer")
                        ])
def test_index_title_query(client, title, location, result):
    # with app.test_client() as c:
    res = client.get("/?jobTitle={}&location={}".format(title, location))
    assert request.args["jobTitle"] == result

@pytest.mark.parametrize("title, location, result", 
                        [
                            ("software+engineer", "san+jose", "san jose"),
                            ("writer", "new+york", "new york")
                        ])
def test_index_location_query(client, title, location, result):
    # with app.test_client() as c:
    res = client.get("/?jobTitle={}&location={}".format(title, location))
    assert request.args["location"] == result

def dprint(val):
    print("^^^^^^^^^^^^^^^^")
    print(val)
    print("^^^^^^^^^^^^^^^^")

def test_index_response(client):
    res = client.get("/?jobTitle={}&location={}".format("writer", "san jose"))
    data = json.loads(res.data.decode())
    assert type(data) == dict